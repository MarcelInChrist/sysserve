import { useState } from "react";

const useApi = (apiFunc) => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);

  const request = async (route) => {
    setLoading(true);
    try {
      const response = await apiFunc(route);
      setError(false);
      setLoading(false);
      setData(response.data);
    } catch (error) {
      setError(true);
      setLoading(false);
    }

    return await apiFunc(route);
  };

  return { data, error, loading, request };
};

export default useApi;
