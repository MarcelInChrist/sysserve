import React, { useContext, useEffect, useState } from "react";
import { paginate } from "../utils/paginate";

import InputField from "./common/input";
import Loading from "./common/loading";
import Pagination from "./common/pagination";
import Select from "./common/select";
import Table from "./common/table";
import _ from "lodash";

import useApi from "../hooks/useApi";
import { getTodos } from "../services/todoService";

import NavContext from "../contexts/useNavContext";

import "../scss/homepage.css";

const HomePage = () => {
  const columns = [
    {
      path: "id",
      order: "asc",
      label: "Todo ID",
    },
    {
      path: "userId",
      order: "asc",
      label: "User ID",
    },
    {
      path: "title",
      order: "asc",
      label: "Title",
    },
    {
      path: "completed",
      order: "asc",
      label: "Completed",
      content: (todo) => (todo.completed ? "Completed" : "Not completed"),
    },
  ];

  const categories = [
    {
      id: "",
      name: "All Todos",
    },
    {
      id: "1",
      name: "Completed",
    },
    {
      id: "2",
      name: "Not Completed",
    },
  ];

  //encapsulate data fetch in api hook
  const getDataApi = useApi(getTodos);

  const [selectedCategory, setSelected] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [searchQuery, setSearchQuery] = useState("");
  const [itemsPerPage, setItems] = useState(5);
  const [sortColumn, setSortColumn] = useState(columns[0]);

  const { openNav } = useContext(NavContext);

  const pages = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  let filtered = getDataApi.data;

  if (selectedCategory)
    filtered = getDataApi.data.filter((item) =>
      selectedCategory === "1" ? item.completed : !item.completed
    );

  if (searchQuery)
    filtered = getDataApi.data.filter(
      (item) =>
        item.title.toLowerCase().indexOf(searchQuery.toLowerCase()) !== -1
    );

  const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
  const paginatedItems = paginate(sorted, currentPage, itemsPerPage);

  const handleSearch = (event) => {
    setCurrentPage(1);
    setSearchQuery(event.currentTarget.value);
  };

  useEffect(() => {
    getDataApi.request();
  }, []);

  return (
    <div
      className={openNav ? "container-fluid nav__open" : "container-fluid"}
      id="homepage"
    >
      <div className="row">
        <Loading
          visible={getDataApi.loading}
          message="Please wait, fetching todos..."
        />
        {/* {openNav && ( */}
        <div className="col-md-2 bg-light">
          <div className="categories">
            {categories.map((category) => (
              <button
                onClick={() => setSelected(category.id)}
                className={
                  category.id === selectedCategory
                    ? "category selected_category animate-slide-in-right"
                    : "category animate-slide-in-right"
                }
                key={category.name}
              >
                <i className="fa fa-info-circle p-3"></i>
                <span>{category.name}</span>
              </button>
            ))}
          </div>
        </div>
        {/* )} */}
        <div id="main-body" className="col-md">
          <div id="filter-container">
            <div className="bg-light p-2 border my-3">
              <InputField
                value={searchQuery}
                name="filter"
                onChange={handleSearch}
                placeholder="Search todo by title"
              />
            </div>

            <div className="bg-light p-2 border my-3 d-flex ">
              <Select
                options={pages}
                label="no. of items to show"
                onChange={(event) =>
                  setItems(parseInt(event.currentTarget.value))
                }
              />
              <button
                title="Refresh todos"
                onClick={getDataApi.request}
                id="refresh-todos"
                className="border"
              >
                <i className="fa fa-refresh"></i>
              </button>
            </div>
          </div>

          <div className="bg-light p-2 border">
            <Table
              sortColumn={sortColumn}
              columns={columns}
              onSort={setSortColumn}
              data={paginatedItems}
            />
          </div>

          <Pagination
            itemsCount={getDataApi.data.length}
            pageSize={itemsPerPage}
            currentPage={currentPage}
            onPageChange={setCurrentPage}
          />
        </div>
      </div>
    </div>
  );
};

export default HomePage;
