import React, { useContext } from "react";
import NavContext from "../contexts/useNavContext";

const NavBar = () => {
  const { openNav, setOpenNav } = useContext(NavContext);

  return (
    <header>
      <div className="header">
        syssverve<span>solutions</span>
      </div>
      <div
        className={openNav ? "menu-btn open" : "menu-btn"}
        onClick={() => setOpenNav((prev) => !prev)}
      >
        <div className="menu-btn__burger"></div>
      </div>
    </header>
  );
};

export default NavBar;
