import React, { useState } from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { paginate } from "../../utils/paginate";

const Pagination = ({ itemsCount, pageSize, currentPage, onPageChange }) => {
  const [currentIndex, setCurrentIndex] = useState(1);

  const pagesCount = Math.ceil(itemsCount / pageSize);

  if (pagesCount <= 1) return null;

  const pages = _.range(1, pagesCount + 1);

  const totalIndex = Math.ceil(pages.length / 3);

  const pagesToDisplay = paginate(pages, currentIndex, 3);

  const renderPrevious = () => {
    if (currentIndex > 1)
      return (
        <li className="pagination-item">
          <button
            className="pagination-btn"
            onClick={() => setCurrentIndex(currentIndex - 1)}
          >
            &laquo;
          </button>
        </li>
      );

    return null;
  };

  const renderNext = () => {
    if (currentIndex < totalIndex)
      return (
        <li className="pagination-item">
          <button
            className="pagination-btn"
            onClick={() => setCurrentIndex(currentIndex + 1)}
          >
            &raquo;
          </button>
        </li>
      );
    return null;
  };

  return (
    <nav className="pagination-items">
      <ul className="pagination bg-light border p-2">
        {renderPrevious()}
        {pagesToDisplay.map((page) => (
          <li
            key={page}
            className={
              page === currentPage
                ? "pagination-item active"
                : "pagination-item "
            }
          >
            <button
              className="pagination-btn"
              onClick={() => onPageChange(page)}
            >
              {page}
            </button>
          </li>
        ))}
        {renderNext()}
      </ul>
    </nav>
  );
};

Pagination.propTypes = {
  itemsCount: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
};

export default Pagination;
