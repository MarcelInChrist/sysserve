import React from "react";

import "../../scss/loading.css";

const Loading = ({ message, visible = false }) => {
  if (!visible) return null;

  return (
    <div id="loading-page">
      <div className="text-center">
        <p className="lead text-muted">{message}</p>
        <div className="spinner-border quip-color" role="status">
          <span className="sr-only"></span>
        </div>
      </div>
    </div>
  );
};

export default Loading;
