import React from "react";

const InputField = ({ value, name, onChange, placeholder }) => {
  return (
    <input
      className="form-field"
      name={name}
      value={value}
      onChange={onChange}
      placeholder={placeholder}
    />
  );
};

export default InputField;
