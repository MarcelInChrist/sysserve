import React from "react";

const Select = ({ name, label, options, ...rest }) => {
  return (
    <select name={name} {...rest} className="form-field">
      <option value="">Select {label}</option>
      {options.map((option) => (
        <option key={option} value={option}>
          {option}
        </option>
      ))}
    </select>
  );
};

export default Select;
