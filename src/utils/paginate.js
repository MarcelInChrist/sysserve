export function paginate(items, currentPageNumber, totalPages) {
  const startIndex = (currentPageNumber - 1) * totalPages;
  return items.slice(startIndex).filter((_, index) => index < totalPages);
}
