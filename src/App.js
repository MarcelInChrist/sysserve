import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";

import NavContext from "./contexts/useNavContext.js";

import HomePage from "./components/homePage.jsx";
import NavBar from "./components/navBar.jsx";

import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.css";

function App() {
  const [openNav, setOpenNav] = React.useState(false);

  return (
    <React.Fragment>
      <NavContext.Provider value={{ openNav, setOpenNav }}>
        <NavBar />
        <main>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Redirect to="/" />
          </Switch>
        </main>
      </NavContext.Provider>
    </React.Fragment>
  );
}

export default App;
