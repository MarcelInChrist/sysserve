import httpService from "./httpService";

export const getTodos = () => httpService.get("/todos");
